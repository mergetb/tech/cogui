
const d3 = require("d3");
const axios = require('axios').default;

/* Data format example

const data = {

    nodes: [
        { id: 'a' },
        { id: 'b' },
    ],

    links: [
        { source: 'a', target: 'b' },
    ]

}

*/

const width = 600;
const height = 600;

function drag(simulation) {


    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    return d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);

}

function linkplan(mzid) {

    axios
        .get(location.origin+'/api/materializations/'+mzid+'/linkplan/xp')
        .then(response => draw_linkplan(response.data))

}

function draw_linkplan(lp) {

    const links = lp.links.map(d => Object.create(d));
    const nodes = lp.nodes.map(d => Object.create(d));


    const simulation = d3.forceSimulation(nodes)
        .force("link", d3.forceLink(links).id(d => d.id))
        .force("charge", d3.forceManyBody())
        .force("center", d3.forceCenter(width/2, height/2));

    const svg = d3.select("#graph").append("svg")
        .attr("viewBox", [0, 0, width, height]);

    const link = svg.append("g")
        .attr("stroke", "#999")
        .attr("stroke-opacity", 0.6)
        .selectAll("line")
        .data(links)
        .join("line")
        .attr("stroke-width", d => Math.sqrt(d.value));

    const node = svg.append("g")
        .attr("stroke", "#fff")
        .attr("stroke-width", 1.5)
        .selectAll("circle")
        .data(nodes)
        .join("g")
        .call(drag(simulation));

    node.append("circle")
        .attr("r", 5)
        .attr("fill", (d) => d.color );

    node.append("title")
        .text(d => d.id);

    node.append("text")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d) { return d.label });

    simulation.on("tick", () => {
        link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);

        node
            .attr("transform", d => "translate("+d.x+","+d.y+")")
    });

    //invalidation.then(() => simulation.stop());

    return svg.node();


}


export default linkplan
