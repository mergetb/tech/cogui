import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/materializations',
          name: 'materializations',
          component: () => import(/* webpackChunkName: "demo" */ './views/Materializations.vue')
        },
        {
          path: '/images',
          name: 'images',
          component: () => import(/* webpackChunkName: "demo" */ './views/Images.vue')
        },
        {
          path: '/nodes',
          name: 'nodes',
          component: () => import(/* webpackChunkName: "demo" */ './views/Nodes.vue')
        },
        {
          path: '/networks',
          name: 'networks',
          component: () => import(/* webpackChunkName: "demo" */ './views/Networks.vue')
        },
        {
          path: '/emulators',
          name: 'emulators',
          component: () => import(/* webpackChunkName: "demo" */ './views/Emulators.vue')
        },
        {
          path: '/simulators',
          name: 'simulators',
          component: () => import(/* webpackChunkName: "demo" */ './views/Simulators.vue')
        },
        {
          path: '/storage',
          name: 'storage',
          component: () => import(/* webpackChunkName: "demo" */ './views/Storage.vue')
        },
        {
          path: '/rex',
          name: 'rex',
          component: () => import(/* webpackChunkName: "demo" */ './views/Rex.vue')
        },
        {
          path: '/driver',
          name: 'driver',
          component: () => import(/* webpackChunkName: "demo" */ './views/Driver.vue')
        },
        {
          path: '/materialization/tasks/:mzid',
          name: 'Materialization Tasks',
          component: () => import(/* webpackChunkName: "demo" */ './views/MzTasks.vue')
        },
        {
          path: '/materialization/infranet/:mzid',
          name: 'Materialization Infranet',
          component: () => import(/* webpackChunkName: "demo" */ './views/MzInfranet.vue')
        },
        {
          path: '/materialization/xpnet/:mzid',
          name: 'Materialization Experiment Network',
          component: () => import(/* webpackChunkName: "demo" */ './views/MzXpnet.vue')
        },
      ]
    },
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
    }
  ]
})
