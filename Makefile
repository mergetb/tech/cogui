prefix ?= /usr/local

VERSION = $(shell git describe --always --long --dirty --tags)
LDFLAGS = "-X cogui.Version=$(VERSION)"

.PHONY: all
all: server client

.PHONY: server
server: build/cogui

build/cogui: server/main.go server/*.go
	$(go-build)

.PHONY: client
client: build/dist

build/dist: dashboard/node_modules
	cd dashboard; npm run build
	mv dashboard/dist build/

dashboard/node_modules: dashboard/package.json dashboard/package-lock.json
	cd dashboard; npm install

.PHONY: clean
clean:
	rm -rf build

.PHONY: install
install: build/cogui
	install -D build/cogui $(DESTDIR)$(prefix)/bin/cogui
	mkdir -p $(DESTDIR)/var/cogui/dist
	cp -r build/dist/* $(DESTDIR)/var/cogui/dist/

include merge.mk
