BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $(dir $<)*.go
endef

define go-build-file
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $<
endef

define go-build-test
	$(call build-slug,go-test)
	$(QUIET) go test -o $@ -c $<
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) protoc \
		-I . \
		-I ./vendor \
		-I $(GOPATH)/src \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef

tools: .tools/golangci-lint

.tools/golangci-lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b .tools v1.24.0
