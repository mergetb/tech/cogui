module cogui

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.6.2
	github.com/sirupsen/logrus v1.5.0
	gitlab.com/mergetb/tech/cogs v0.7.5
	gitlab.com/mergetb/tech/wgd v0.0.14
	gitlab.com/mergetb/xir v0.2.13
	google.golang.org/grpc v1.26.0
)
