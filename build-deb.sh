#!/bin/bash

set -e

debuild \
    --build-hook="make tools" \
    -e V=1 -e prefix=/usr -e arch=amd64 \
    $DEBUILD_ARGS -aamd64 -i -us -uc -b

mkdir -p build

mv ../cogui*.build* build/
mv ../cogui*.changes build/
mv ../cogui*.deb build/

