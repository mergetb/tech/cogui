package main

import (
	"log"
	"sort"
	"strings"
	"time"

	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

type Output struct {
	Timestamp    string
	Mzid         string
	Taskid       string
	Stageid      string
	StageOrder   int
	Actionid     string
	ActionKind   string
	Instance     string
	ActionName   string
	Dependencies []string
	Completed    bool
	Error        string
	Masked       bool
}

func listTasks(name string, all bool) ([]*Output, error) {

	tasks, err := task.ListTasks(name)
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(tasks, func(i, j int) bool {

		t0, _ := time.Parse(task.TimeFormat, tasks[i].Timestamp)
		t1, _ := time.Parse(task.TimeFormat, tasks[j].Timestamp)

		return t0.Before(t1)

	})

	ordered := make([]*Output, 0)
	stageMap := make(map[string]*task.Stage)
	for _, t := range tasks {
		stages, err := task.GetTaskStages(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, s := range stages {
			stageMap[s.ID] = s
		}
		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, a := range actions {

			as, err := task.GetActionStatus(a.Mzid, t.ID, a.ID)
			if err != nil {
				log.Println("Error:", err)
				continue
			}

			if !strings.HasPrefix(a.Mzid, name) {
				continue
			}

			if a == nil || a.ID == "" {
				continue
			}

			errmsg := ""
			if as.Error != nil {
				errmsg = *as.Error
			}

			if as.Complete && !all {
				continue
			}

			_, ok := stageMap[a.Stageid]
			if !ok {
				log.Printf("Error: action missing stage: %v %v\n", a, stageMap)
				continue
			}

			ordered = append(ordered, &Output{
				Timestamp:    t.Timestamp,
				Mzid:         a.Mzid,
				Taskid:       t.ID,
				Stageid:      a.Stageid,
				StageOrder:   stageMap[a.Stageid].Order,
				Actionid:     a.ID,
				ActionKind:   a.Kind,
				Instance:     a.MzInstance,
				ActionName:   a.Action,
				Dependencies: t.Deps,
				Completed:    as.Complete,
				Error:        errmsg,
				Masked:       as.Masked,
			})
		}
	}

	order := Sorted(ordered)

	return order, nil

}

func Sorted(output []*Output) []*Output {
	sort.Slice(output, func(i, j int) bool {
		t0, _ := time.Parse(task.TimeFormat, output[i].Timestamp)
		t1, _ := time.Parse(task.TimeFormat, output[j].Timestamp)

		// sort by timestamp first
		if t0.Before(t1) && !t0.Equal(t1) {
			return true
		}
		if t0.After(t1) && !t0.Equal(t1) {
			return false
		}

		// then sort by stage order
		if output[i].StageOrder < output[j].StageOrder {
			return true
		}
		if output[i].StageOrder > output[j].StageOrder {
			return false
		}

		// finally sort by action id
		return output[i].Actionid > output[j].Actionid
	})
	return output
}
