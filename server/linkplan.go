package main

import (
	"fmt"

	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func lpnet(topo *xir.Net, lp *fabric.LinkPlan, mzid string) (*xir.Net, error) {

	nodeInfo, err := task.ListNodeInfo(mzid)
	if err != nil {
		return nil, fmt.Errorf("failed to list nodes for %s: %v", mzid, err)
	}

	names := make(map[string]string)
	for _, x := range nodeInfo {
		names[x.Machine] = x.XpName
	}

	lpn := xir.NewNet("lpn")

	for host, ports := range lp.TrunkMap {

		_, _, n := topo.GetNode(host)
		if n == nil {
			return nil, fmt.Errorf("node %s not found", host)
		}

		_, _, x := lpn.GetNode(host)
		if x == nil {
			c := n.Clone()
			xpname, ok := names[c.Label()]
			if ok {
				c.Props["xpname"] = xpname
			}
			lpn.AddNode(c)
		}

		for portName, port := range ports {

			e := n.GetEndpoint(portName)
			if e == nil {
				return nil, fmt.Errorf("endpoint %s:%s not found", host, port)
			}

			for _, nbr := range e.Neighbors {

				_, _, x := lpn.GetNode(nbr.Endpoint.Parent.Label())
				if x == nil {
					c := nbr.Endpoint.Parent.Clone()
					xpname, ok := names[c.Label()]
					if ok {
						c.Props["xpname"] = xpname
					}
					lpn.AddNode(c)
				}
				lpn.Link(e, nbr.Endpoint)

			}

		}

	}

	for host, ports := range lp.AccessMap {

		//TODO total copy pasta
		_, _, n := topo.GetNode(host)
		if n == nil {
			return nil, fmt.Errorf("node %s not found", host)
		}

		_, _, x := lpn.GetNode(host)
		if x == nil {
			c := n.Clone()
			xpname, ok := names[c.Label()]
			if ok {
				c.Props["xpname"] = xpname
			}
			lpn.AddNode(c)
		}

		for portName, port := range ports {

			e := n.GetEndpoint(portName)
			if e == nil {
				return nil, fmt.Errorf("endpoint %s:%s not found", host, port)
			}

			for _, nbr := range e.Neighbors {

				_, _, x := lpn.GetNode(nbr.Endpoint.Parent.Label())
				if x == nil {
					c := nbr.Endpoint.Parent.Clone()
					xpname, ok := names[c.Label()]
					if ok {
						c.Props["xpname"] = xpname
					}
					lpn.AddNode(c)
				}
				lpn.Link(e, nbr.Endpoint)

			}

		}

	}

	return lpn, nil

}
