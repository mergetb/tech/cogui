package main

import (
	"context"
	"strings"

	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/wgd/api/wgd"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type WgdSummary struct {
	Count            int `json:"count"`
	Materializations int `json:"materializations"`
}

func wgdSummary() (*WgdSummary, error) {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		return nil, err
	}

	creds, err := credentials.NewClientTLSFromFile("/etc/wgd/wgd.pem", "")
	if err != nil {
		return nil, err
	}

	conn, err := grpc.Dial("localhost:36000", grpc.WithTransportCredentials(creds))
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	wgc := wgd.NewWgdClient(conn)

	result := new(WgdSummary)

	for _, mz := range mzs {

		resp, err := wgc.InterfaceStatus(context.TODO(), &wgd.InterfaceStatusReq{
			Enclaveid: mz.Mzid,
			Namespace: &wgd.WgNs{
				Code: wgd.WgNsCode_Namespace,
				Key:  mz.Mzid,
			},
		})

		if err != nil {
			return nil, err
		}

		if resp.Wgstatus != "" {

			result.Materializations++

			result.Count += strings.Count(resp.Wgstatus, "peer:")
		}
	}

	return result, nil

}
