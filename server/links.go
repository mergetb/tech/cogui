package main

import (
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

type LinkSummary struct {
	Total    int `json:"total"`
	Emulated int `json:"emulated"`
}

func linkSummary() (*LinkSummary, error) {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		return nil, err
	}

	result := new(LinkSummary)

	for _, x := range mzs {

		li, err := task.ListLinkInfos(x.Mzid)
		if err != nil {
			return nil, err
		}

		for _, i := range li {
			result.Total++
			if i.Kind == task.MoaLink {
				result.Emulated++
			}
		}

	}

	return result, nil

}
