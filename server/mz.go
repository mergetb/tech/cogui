package main

import (
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"time"
)

type MzSummary struct {
	Info                             *task.MzInfo
	Nodes                            int
	Links                            int
	Age                              int
	Total, Completed, Pending, Error int
}

func mzSummaries() ([]*MzSummary, error) {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		return nil, err
	}

	var result []*MzSummary

	for _, mzi := range mzs {

		summary := &MzSummary{Info: mzi}

		tasks, err := listTasks(mzi.Mzid, true)
		if err != nil {
			return nil, err
		}

		ni, err := task.ListNodeInfo(mzi.Mzid)
		if err != nil {
			return nil, err
		}
		summary.Nodes = len(ni)

		li, err := task.ListLinkInfos(mzi.Mzid)
		if err != nil {
			return nil, err
		}
		summary.Links = len(li)

		t := time.Now()

		for _, x := range tasks {

			xt, _ := time.Parse(task.TimeFormat, x.Timestamp)
			if xt.Before(t) {
				t = xt
			}

			summary.Total++

			if x.Completed {
				summary.Completed++
			} else if x.Error != "" {
				summary.Error++
			} else {
				summary.Pending++
			}

		}

		summary.Age = int(time.Now().Sub(t).Seconds())

		result = append(result, summary)

	}

	return result, nil

}
