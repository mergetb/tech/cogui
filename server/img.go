package main

import (
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

type ImageSummary struct {
	Total int
	Table map[string]int
}

func NewImageSummary() *ImageSummary {
	return &ImageSummary{
		Table: make(map[string]int),
	}
}

func imageSummary() (*ImageSummary, error) {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		return nil, err
	}

	result := NewImageSummary()

	for _, x := range mzs {

		ni, err := task.ListNodeInfo(x.Mzid)
		if err != nil {
			return nil, err
		}

		for _, i := range ni {

			result.Total++
			result.Table[i.Image.Image.Name]++

		}
	}

	return result, nil
}
