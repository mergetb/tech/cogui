package main

import (
	"fmt"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type D3net struct {
	Nodes []*D3node `json:"nodes"`
	Links []*D3link `json:"links"`
}

type D3node struct {
	Id    string `json:"id"`
	Label string `json:"label"`
	Color string `json:"color"`
}

type D3link struct {
	Source string `json:"source"`
	Target string `json:"target"`
}

func d3net(topo *xir.Net) *D3net {

	d3n := new(D3net)

	for _, x := range topo.AllNodes() {

		label := x.Label()
		xpname, ok := x.Props["xpname"].(string)
		if ok {
			label = fmt.Sprintf("%s (%s)", x.Label(), xpname)
		}

		color := "#222"
		if tb.HasRole(x, tb.XpSwitch) {
			color = "#2c51a8"
		}

		d3n.Nodes = append(d3n.Nodes, &D3node{
			Id:    x.Label(),
			Label: label,
			Color: color,
		})
	}

	for _, x := range topo.AllLinks() {
		d3n.Links = append(d3n.Links, &D3link{
			Source: x.Endpoints[0].Endpoint.Parent.Label(),
			Target: x.Endpoints[1].Endpoint.Parent.Label(),
		})
	}

	return d3n

}
