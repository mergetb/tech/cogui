package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

var (
	Version = "undefined"
	cfgFile = "/etc/cogui.yml"
)

func main() {

	root := &cobra.Command{
		Use:   "cogui",
		Short: "The Cogs web UI server",
		Run:   func(cmd *cobra.Command, args []string) { run() },
	}
	root.PersistentFlags().StringVar(&cfgFile, "config", "/etc/cogui.yml", "config file to use")
	root.PersistentFlags().StringP("address", "a", "0.0.0.0", "address to listen on")
	root.PersistentFlags().IntP("port", "p", 8080, "port to listen on")
	root.PersistentFlags().StringP("xir", "x", "/etc/cogs/tb-xir.json", "testbed xir path")
	viper.BindPFlag("address", root.PersistentFlags().Lookup("address"))
	viper.BindPFlag("port", root.PersistentFlags().Lookup("port"))
	viper.BindPFlag("xir", root.PersistentFlags().Lookup("xir"))
	viper.SetDefault("address", "localhost")
	viper.SetDefault("xir", "/etc/cogs/tb-xir.json")
	viper.SetDefault("port", 8080)

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath("/etc")
		viper.SetConfigName("cogui.yml")
	}

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		log.WithError(err).Warn("could not open config file, using defaults")
	}

	root.Execute()

}

func run() {

	flag.Parse()

	cfg := runtime.GetConfig(false)
	common.SetEtcdConfig(cfg.Etcd)

	tbxir, err := xir.FromFile(viper.GetString("xir"))
	if err != nil {
		log.Fatal(err)
	}

	r := gin.Default()
	r.Use(cors.Default())

	r.Use(static.Serve("/", static.LocalFile(".", true)))

	api := r.Group("/api")
	{

		api.GET("/xir", func(c *gin.Context) {
			c.JSON(http.StatusOK, tbxir)
		})

		api.GET("api/tasks", func(c *gin.Context) {

			qall := c.Query("all")
			all := false
			if qall == "true" {
				all = true
			}

			tasks, err := listTasks("", all)
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, tasks)

		})

		api.GET("/nodes", func(c *gin.Context) {

			nodes, err := task.ListNodes()
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, nodes)

		})

		api.GET("/links", func(c *gin.Context) {

			ls, err := linkSummary()
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, ls)

		})

		api.GET("/materializations", func(c *gin.Context) {

			mzs, err := mzSummaries()
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, mzs)
		})

		api.GET("/materializations/:name/tasks", func(c *gin.Context) {

			name := c.Param("name")
			tasks, err := listTasks(name, true)
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, tasks)
		})

		api.GET("/image/usage", func(c *gin.Context) {

			is, err := imageSummary()
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, is)

		})

		api.GET("/materializations/:name/linkplan/xp", func(c *gin.Context) {

			name := c.Param("name")
			lp, err := fabric.FetchLinkPlan(name, fabric.ExperimentLP)
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			lpn, err := lpnet(tbxir, lp, name)
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, d3net(lpn))
		})

		api.GET("/wgd/summary", func(c *gin.Context) {

			ls, err := wgdSummary()
			if err != nil {
				log.Error(err)
				c.AbortWithStatus(500)
				return
			}

			c.JSON(http.StatusOK, ls)

		})

	}

	listen := fmt.Sprintf("%s:%d",
		viper.GetString("address"),
		viper.GetInt("port"),
	)
	log.Fatal(r.Run(listen))

}
