# Cogs Web Dashboard

This is a simple web-based dashboard for the Cogs

![](cogui.png)

There are two components

- [A Golang web server](server)
- [A Vue.js Dashboard](dashboard)

The are packaged together as a single Debian package. The web server serves the
web application as well as the API used by web clients.

## Configuration

### Listening

By default cogui will listen on `localhost:8080`. You can update the listening
configuration by creating a file called `/etc/cogui.yml` like the following

```yaml
address: 10.10.0.3
port: 80
```

These values can also be provided as flags to the `cogui` utility.

### Testbed XIR

`cogui` needs to know where the testbed model lives. By default it will look for
`/etc/cogs/tb-xir.json`, you can specify an alternative in `/etc/cogui.yml`

```yaml
xir: /opt/cogs/my-testbed.json
```

