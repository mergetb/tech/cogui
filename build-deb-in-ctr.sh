#!/bin/bash

set -e

docker build -f debian/builder.dock -t cogui-builder .
docker run -v `pwd`:/cogui cogui-builder /cogui/build-deb.sh
